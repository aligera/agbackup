#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests,json,sys,argparse,time

#/api/system/config/export/<nome da config>
#/api/system/configs pra consultar configs

# Zabbix external check:
# Command: agosbackup[{HOST.IP},admin,aligera,-d,/tmp,-f,{HOST.NAME}]
def get_file_names(agos_data):
    url=agos_data['url']+'/api/system/configs'
    username=agos_data['username']
    password=agos_data['password']
    a=requests.get(url,auth=(username,password))
    if a.status_code == 200:
        try:
            data = a.json()
            return data
        except:
            return False
    else:
        return False

def get_file_content(agos_data,filename):
    url=agos_data['url']+'/api/system/config/export/'+filename
    username=agos_data['username']
    password=agos_data['password']
    a=requests.get(url,auth=(username,password))
    if a.status_code == 200:
        data = a.content
        return data
    else:
        return False

def write_file(name,content):
    f=open(name,'w+b')
    g=content
    if f.write(g):
        f.close()
        return True
    else:
        return False

if __name__ == '__main__':
    now_date=time.strftime('%Y%m%d%H%M%S')
    parser = argparse.ArgumentParser()
    parser.add_argument('address',help="IP or hostname")
    parser.add_argument('username',help="Username")
    parser.add_argument('password',help="Password")
    parser.add_argument('-d','--directory',help="Directory to save files")
    parser.add_argument('-f','--file',help="Output filename")
    args = parser.parse_args()
    address='http://'+args.address
    username=args.username
    password=args.password
    host_data = {'url':address,'username':username,'password':password}
    file_names=get_file_names(host_data)
    if file_names is not False:
        file_names_dict = file_names['saved_configurations']
        file_names_dict.sort(reverse=True)
        download_file=get_file_content(host_data,file_names_dict[0])
        if download_file is not False:
            new_name = file_names_dict[0]
            new_name = new_name.replace(' ','_')
            new_name = new_name.replace(':','')
            new_name = args.address+'_'+new_name
            if args.file:
                new_name = args.file
            if args.directory:
                new_name = args.directory+'/'+new_name
            if write_file(new_name+'_'+now_date+'.conf',download_file):
                print(0)
                sys.exit(0)
            else:
                print(1)
                sys.exit(1)
        else:
            print(1)
            sys.exit(1)
    else:
        print(1)
        sys.exit(1)