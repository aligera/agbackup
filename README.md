# Aligera AGOS Backup
Ferramenta de backup de configurações para a plataforma AGOS.

Salva último arquivo de configuração do gateway em um diretório local.

A concepção inicial da aplicação foi para utilização a partir do Zabbix, porém pode ser utilizada a partir de qualquer outra ferramenta com as caracteristicas de execução de scripts equivalentes ao Zabbix, ou ainda executada de forma standalone a partir de um servidor Linux.

A ferramenta utiliza a API do AGOS com requisições HTTP na porta 80.

## Requisitos:
 - Python3
 - Biblioteca python requests

## Como usar:
 - Copiar o programa agosbackup.py para o diretório de scripts do Zabbix:
     - Em uma instalação padrão, o diretório de scripts é /usr/lib/zabbix/externalscripts/
     - Ajustar permissões:
         - ``` chmod 777 /usr/lib/zabbix/externalscripts/agosbackup.py ```
 - Criar o diretório de destino dos arquivos e ajustar as permissões:
     - ``` mkdir /var/agosbackup ```
     - ``` chmod 666 /var/agosbackup -R ```
 - Criar o script a partir da interface do Zabbix:
     - Administration > Scripts > Create Script
         - Name: agosbackup
         - Type: script
         - Execute on: Zabbix Server
         - Commands: agosbackup[{HOST.IP},<USER>,<PASSWORD>,-d,<DIR>,-f,{HOST.NAME}]
             - Parâmetros do comandos acima:
	             - USER: Nome de usuário para acesso ao equipamento
                 - PASSWORD: Senha para acesso ao equipamento
                 - DIR: Diretório de destino dos arquivos de backup
                 - Exemplo: ``` agosbackup.py[{HOST.IP},admin,aligera,-d,/var/agosbackup,-f,{HOST.NAME}] ```
         - User group: All
         - Host group: All
         - Required host permissions: Write

Com a execução dos procedimentos acima, o script pode ser utilizado a partir do Zabbix.

## Limitações
 - O script recupera somente arquivos que estejam com a nomenclatura padrão de arquivos de configuração do AGOS (Ex.: Activation - 2021-03-17 16:42:06)
 - Sempre será recuperado o último arquivo de configuração, independentemente do arquivo que estiver ativo.

